@extends('layouts.app')

@section('title')
    Users
@endsection

@section('breadcrumb')
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Compte client</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Nouveaux clients</li>
                </ol>
            </nav>
        </div>
    </div>
    <!--end breadcrumb-->
@endsection


@section('content')
    <div class="row">
        <div class="col-xl-10 mx-auto">
            <hr/>
            <div class="card border-top border-0 border-4 border-info">
                <div class="card-body">
                    <form action="{{ route('customers.store') }}" method="post">
                        @csrf
                        <div class="border p-4 rounded">
                            <div class="card-title d-flex align-items-center">
                                <div><i class="bx bxs-user me-1 font-22 text-info"></i>
                                </div>
                                <h5 class="mb-0 text-info">Ajout d'un nouveau client</h5>
                            </div>
                            <hr/>
                            <div class="row mb-3">
                                <label for="reference" class="col-sm-3 col-form-label">REFERENCE</label>
                                <div class="col-sm-9">
                                    <input type="text" name="" disabled class="form-control" id="reference" placeholder="Référence">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="name" class="col-sm-3 col-form-label">Nom</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" value="{{ old('name') }}" class="form-control" id="name" placeholder="Nom & prénoms" required>
                                    @error('name')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="customer_wallet_id" class="col-sm-3 col-form-label">PORTEFEUILLE CLT</label>
                                <div class="col-sm-9">
                                    <select name="customere_wallet_id" id="customer_wallet_id" class="form-control" >
                                        <option value="">Choisir...</option>
                                        @foreach($wallets as $wallet)
                                            <option value="{{ $wallet->id }}">{{ $wallet->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('customer_wallet_id')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="community_number" class="col-sm-3 col-form-label">N° INTRACOMMUNAUTAIRE</label>
                                <div class="col-sm-9">
                                    <input type="text" name="community_number" value="{{ old('community_number') }}" class="form-control" id="community_number" placeholder="Numéro..." required>
                                    @error('community_number')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="email" class="col-sm-3 col-form-label">EMAIL</label>
                                <div class="col-sm-9">
                                    <input type="email" value="{{ old('email') }}" name="email" class="form-control" id="email" placeholder="jean@gmail.com" required>
                                    @error('email')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="phone" class="col-sm-3 col-form-label">TELEPHONE</label>
                                <div class="col-sm-9">
                                    <input type="tel" value="{{ old('phone') }}" maxlength="10" minlength="10" name="phone" class="form-control" id="phone" placeholder="2245635478" required>
                                    @error('phone')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="phone2" class="col-sm-3 col-form-label">PORTABLE</label>
                                <div class="col-sm-9">
                                    <input type="tel" value="{{ old('phone2') }}" maxlength="10" minlength="10" name="phone2" class="form-control" id="phone2" placeholder="0102040809" required>
                                    @error('phone2')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="postal_code" class="col-sm-3 col-form-label">CODE POSTAL</label>
                                <div class="col-sm-9">
                                    <input type="text" value="{{ old('postal_code') }}" name="postal_code" class="form-control" id="postal_code" placeholder="Code postal" required>
                                    @error('postal_code')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="caution" class="col-sm-3 col-form-label">CAUTION</label>
                                <div class="col-sm-9">
                                    <input type="number" value="{{ old('caution') }}" min="0" name="caution" class="form-control" id="caution" placeholder="caution" required>
                                    @error('caution')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="threshold" class="col-sm-3 col-form-label">PLAFOND CREANCE</label>
                                <div class="col-sm-9">
                                    <input type="number" value="{{ old('threshold') }}" min="0" name="threshold" class="form-control" id="threshold" placeholder="plafond créance" required>
                                    @error('threshold')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="location" class="col-sm-3 col-form-label">LOCALISATION</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="location" id="location" cols="30" rows="5" placeholder="Yopougon, Maroc" required>{{ old('location') }}</textarea>
                                    @error('location')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    <button type="submit" class="btn btn-info px-5 text-white">Ajouter</button>
                                    <button type="reset" class="btn btn-info px-5 text-white">Annuler</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
