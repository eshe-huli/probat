@extends('layouts.app')

@section('title')
    Users
@endSection

@section('breadcrumb')
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Gestion stock ventes</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Fournisseurs</li>
                </ol>
            </nav>
        </div>
    </div>
    <!--end breadcrumb-->
@endsection


@section('content')
    <div>
        <h6 class="mb-0 text-uppercase">Liste des Fournisseurs</h6>
        <div class="bx-pull-right mb-0 text-uppercase">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal"
                    data-bs-target="#exampleVerticallycenteredModal">
                <i class="bx bx-plus-circle"></i>
                Ajouter
            </button>
        </div>
    </div>

    <hr/>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="example2" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Référence</th>
                        <th>Nom</th>
                        <th>Email</th>
                        <th>Téléphone</th>
                        <th>Localisation</th>
                        <th>Ajouté le</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($suppliers as $supplier)
                        <tr>
                            <td>{{ $supplier->reference }}</td>
                            <td>{{ $supplier->name }}</td>
                            <td>{{ $supplier->email }}</td>
                            <td>{{ $supplier->phone }}</td>
                            <td>{{ $supplier->location }}</td>
                            <td>{{ \Carbon\Carbon::parse($supplier->created_at)->format("d-m-Y H:i:s") }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Référence</th>
                        <th>Nom</th>
                        <th>Email</th>
                        <th>Téléphone</th>
                        <th>Localisation</th>
                        <th>Ajouté le</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col">
        <!-- Modal -->
        <div class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ajouter un fournisseur</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <form action="{{ route('suppliers.store') }}" method="post">
                        <div class="modal-body">
                            @csrf
                            <div class="border p-4 rounded">
                                <div class="card-title d-flex align-items-center">
                                    <h5 class="mb-0 text-info">ARTICLES</h5>
                                </div>
                                <hr/>
                                <div class="row mb-3">
                                    <label for="reference" class="col-sm-3 col-form-label">Reference</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('reference') }}" min="0" name="reference"
                                               class="form-control" id="reference" placeholder="Reference" required>
                                        @error('reference')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="name" class="col-sm-3 col-form-label">Nom</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('name') }}" min="0" name="name"
                                               class="form-control" id="name" placeholder="Nom" required>
                                        @error('name')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="email" class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" value="{{ old('email') }}" min="0" name="email"
                                               class="form-control" id="email" placeholder="Email" required>
                                        @error('email')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="location" class="col-sm-3 col-form-label">Adresse</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('location') }}" min="0" name="location"
                                               class="form-control" id="price" placeholder="Adresse" required>
                                        @error('location')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="phone" class="col-sm-3 col-form-label">Numero</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('phone') }}" min="0" name="phone"
                                               class="form-control" id="phone" placeholder="Numero" required>
                                        @error('phone')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                            <button type="submit" class="btn btn-info px-5 text-white">Enregisstrer</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

@endsection
