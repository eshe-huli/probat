<div class="sidebar-wrapper" data-simplebar="true">
    <div class="sidebar-header">
        <div>
            <img src="{{asset('logo.jpg')}}" class="logo-icon" alt="logo icon">
        </div>
        <div>
            {{--
                        <h4 class="logo-text">Probat</h4>
            --}}
        </div>
        <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
        </div>
    </div>
    <!--navigation-->
    <ul class="metismenu" id="menu">
        <li>
            <a href="{{ route('home') }}">
                <div class="parent-icon"><i class='bx bx-pie-chart-alt-2'></i>
                </div>
                <div class="menu-title">Tableau de board</div>
            </a>
        </li>

        <li class="menu-label">Gestion stocks</li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-money'></i>
                </div>
                <div class="menu-title">Ventes</div>
            </a>
            <ul>
                <li>
                    <a href="{{ route('sales.create',) }}"><i class="bx bx-right-arrow-alt"></i>Vendre</a>
                </li>
                <li>
                    <a href="{{ route('sales.index') }}"><i class="bx bx-right-arrow-alt"></i>Listes des ventes</a>
                </li>
            </ul>
        </li>

        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-package'></i>
                </div>
                <div class="menu-title">Inventaire</div>
            </a>
            <ul>
                <li>
                    <a href="{{ route('articles.index') }}"><i class="bx bx-right-arrow-alt"></i>Articles</a>
                </li>
            </ul>
        </li>

        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-store-alt'></i>
                </div>
                <div class="menu-title">Fournisseurs</div>
            </a>
            <ul>
                <li>
                    <a href="{{ route('suppliers.index') }}"><i class="bx bx-right-arrow-alt"></i>Liste des Fournisseurs</a>
                </li>
            </ul>
        </li>

{{--        <li class="menu-label">Tresorerie</li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bxs-bank'></i>
                </div>
                <div class="menu-title">Banque</div>
            </a>
            <ul>
                <li>
                    <a href="#"><i class="bx bx-right-arrow-alt"></i>Vendre</a>
                </li>
                <li>
                    <a href="#"><i class="bx bx-right-arrow-alt"></i>Listes des ventes</a>
                </li>
            </ul>
        </li>--}}

        <li class="menu-label">Gestion clients</li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-group'></i>
                </div>
                <div class="menu-title">Compte clients</div>
            </a>
            <ul>
                <li>
                    <a href="{{ route('customereWallets.create') }}"><i class="bx bx-right-arrow-alt"></i>Ajouter
                        Portefeuille client</a>
                </li>
                <li>
                    <a href="{{ route('customers.create') }}"><i class="bx bx-right-arrow-alt"></i>Nouveau client</a>
                </li>
                <li>
                    <a href="{{ route('customers.index') }}"><i class="bx bx-right-arrow-alt"></i>Liste client(s)</a>
                </li>
            </ul>
        </li>

        <li class="menu-label">Paramètres</li>

        <li>
            <a href="{{ route('categories.index') }}">
                <div class="parent-icon">
                    <i class='bx bxs-grid-alt'></i>
                </div>
                <div class="menu-title">Categories</div>
            </a>
        </li>

        <li>
            <a href="{{ route('conditionings.index') }}">
                <div class="parent-icon">
                    <i class='bx bxs-package'></i>
                </div>
                <div class="menu-title">Conditionements</div>
            </a>
        </li>

        <li>
            <a href="{{ route('paymentModes.index') }}">
                <div class="parent-icon">
                    <i class='bx bx-money'></i>
                </div>
                <div class="menu-title">Modes de paiement</div>
            </a>
        </li>

        <li class="menu-label"> Gestion utilisateurs</li>

        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bxs-user-detail'></i>
                </div>
                <div class="menu-title">Users</div>
            </a>
            <ul>
                {{--<li>
                    <a href="#"><i class="bx bx-right-arrow-alt"></i>Créer un User</a>
                </li>--}}
                <li>
                    <a href="{{ route('users.index') }}"><i class="bx bx-right-arrow-alt"></i>Liste des Users</a>
                </li>
            </ul>
        </li>
        {{--        <li class="menu-label">Configuration</li>--}}
        {{--        <li>--}}
        {{--            <a href="javascript:;" class="has-arrow">--}}
        {{--                <div class="parent-icon"><i class='bx bx-user-check'></i>--}}
        {{--                </div>--}}
        {{--                <div class="menu-title">Rôles</div>--}}
        {{--            </a>--}}
        {{--            <ul>--}}
        {{--                <li>--}}
        {{--                    <a href="#"><i class="bx bx-right-arrow-alt"></i>Créer un rôle</a>--}}
        {{--                </li>--}}
        {{--                <li>--}}
        {{--                    <a href="#"><i class="bx bx-right-arrow-alt"></i>Liste des rôles</a>--}}
        {{--                </li>--}}
        {{--            </ul>--}}
        {{--        </li>--}}
        {{--        <li>--}}
        {{--            <a href="javascript:;" class="has-arrow">--}}
        {{--                <div class="parent-icon"><i class='bx bx-user-x'></i>--}}
        {{--                </div>--}}
        {{--                <div class="menu-title">Permissions</div>--}}
        {{--            </a>--}}
        {{--            <ul>--}}
        {{--                <li>--}}
        {{--                    <a href="#"><i class="bx bx-right-arrow-alt"></i>Créer une permission</a>--}}
        {{--                </li>--}}
        {{--                <li>--}}
        {{--                    <a href="#"><i class="bx bx-right-arrow-alt"></i>Liste des permissions</a>--}}
        {{--                </li>--}}
        {{--            </ul>--}}
        {{--        </li>--}}
    </ul>
    <!--end navigation-->
</div>
