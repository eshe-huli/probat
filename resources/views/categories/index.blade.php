@extends('layouts.app')

@section('title')
    Categories
@endSection

@section('breadcrumb')
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Paramètres</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Liste categories</li>
                </ol>
            </nav>
        </div>
    </div>
    <!--end breadcrumb-->
@endsection


@section('content')

    <div>
        <h6 class="mb-0 text-uppercase">Liste des categories</h6>
        <div class="bx-pull-right mb-0 text-uppercase">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal"
                    data-bs-target="#exampleVerticallycenteredModal">
                <i class="bx bx-plus-circle"></i>
                Ajouter
            </button>
        </div>
    </div>

    <hr/>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="example2" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Ajoute le</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->name }}</td>
                            <td>{{ \Carbon\Carbon::parse($category->created_at)->format("d-m-Y H:i:s") }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Nom</th>
                        <th>Ajoute le</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col">
        <!-- Modal -->
        <div class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ajouter un mode de conditionement</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <form action="{{ route('categories.store') }}" method="post">
                        <div class="modal-body">
                            @csrf
                            <div class="border p-4 rounded">
                                <div class="card-title d-flex align-items-center">
                                    <h5 class="mb-0 text-info">MODE DE CONDITIONEMENT</h5>
                                </div>
                                <hr/>
                                <div class="row mb-3">
                                    <label for="name" class="col-sm-3 col-form-label">Nom</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('name') }}" min="0" name="name"
                                               class="form-control" id="name" placeholder="Nom" required>
                                        @error('name')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                            <button type="submit" class="btn btn-info px-5 text-white">Enregisstrer</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

@endsection
