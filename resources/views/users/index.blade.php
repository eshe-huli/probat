@extends('layouts.app')

@section('title')
    Users
@endsection

@section('breadcrumb')
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Liste des utilisateurs</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Utilisateurs</li>
                </ol>
            </nav>
        </div>
    </div>
    <!--end breadcrumb-->
@endsection


@section('content')
    <div>
        <h6 class="mb-0 text-uppercase">Liste des utilisateurs</h6>
        <div class="bx-pull-right mb-0 text-uppercase">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal"
                    data-bs-target="#exampleVerticallycenteredModal">
                <i class="bx bx-plus-circle"></i>
                Ajouter
            </button>
        </div>
    </div>

    <hr/>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="example2" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prénoms</th>
                        <th>Email</th>
                        <th>Téléphone</th>
                        <th>Ajouté le</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->first_name }}</td>
                                <td>{{ $user->last_name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>{{ \Carbon\Carbon::parse($user->created_at)->format("d-m-Y H:i:s") }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Nom</th>
                        <th>Prénoms</th>
                        <th>Email</th>
                        <th>Téléphone</th>
                        <th>Ajouté le</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col">
        <!-- Modal -->
        <div class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ajouter un Utilissateurr</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <form action="{{ route('register.post') }}" method="post">
                        <div class="modal-body">
                            @csrf
                            <div class="border p-4 rounded">
                                <div class="card-title d-flex align-items-center">
                                    <h5 class="mb-0 text-info">Utilisateurs</h5>
                                </div>
                                <hr/>
                                <div class="row mb-3">
                                    <label for="first_name" class="col-sm-3 col-form-label">Prenom</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('first_name') }}" min="0" name="first_name"
                                               class="form-control" id="reference" placeholder="Prenom" required>
                                        @error('first_name')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="last_name" class="col-sm-3 col-form-label">Nom</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('last_name') }}" min="0" name="last_name"
                                               class="form-control" id="last_name" placeholder="Nom" required>
                                        @error('last_name')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="username" class="col-sm-3 col-form-label">Username</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('username') }}" min="0" name="username"
                                               class="form-control" id="username" placeholder="Username" required>
                                        @error('username')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="email" class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" value="{{ old('email') }}" min="0" name="email"
                                               class="form-control" id="email" placeholder="Email" required>
                                        @error('email')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="phone" class="col-sm-3 col-form-label">Numero</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('phone') }}" min="0" name="phone"
                                               class="form-control" id="phone" placeholder="Numero" required>
                                        @error('phone')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="password" class="col-sm-3 col-form-label">Mot de passe</label>
                                    <div class="col-sm-9">
                                        <input type="password" value="{{ old('password') }}" min="0" name="password"
                                               class="form-control" id="password" placeholder="********" required>
                                        @error('password')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="password_confirmation" class="col-sm-3 col-form-label">Confirmer le mot de passe</label>
                                    <div class="col-sm-9">
                                        <input type="password" value="{{ old('password_confirmation') }}" min="0" name="password_confirmation"
                                               class="form-control" id="password_confirmation" placeholder="Confirmer lemot de passee" required>
                                        @error('password_confirmation')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                            <button type="submit" class="btn btn-info px-5 text-white">Enregisstrer</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

@endsection
