@extends('layouts.app')

@section('title')
    Articles
@endsection

@section('breadcrumb')
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Liste articles</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Liste articles</li>
                </ol>
            </nav>
        </div>
    </div>
    <!--end breadcrumb-->
@endsection

@section('content')
    <div>
        <h6 class="mb-0 text-uppercase">Liste des articles</h6>
        <div class="bx-pull-right mb-0 text-uppercase">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal"
                    data-bs-target="#exampleVerticallycenteredModal">
                <i class="bx bx-plus-circle"></i>
                Ajouter
            </button>
        </div>
    </div>
    <hr/>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="example2" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Référence</th>
                        <th>Designation</th>
                        <th>Prix</th>
                        <th>Prix HT</th>
                        <th>Prix TTC</th>
                        <th>Conditionement</th>
                        <th>Ajoute le</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articles as $article)
                        <tr>
                            <td>{{ $article->reference }}</td>
                            <td>{{ $article->name }}</td>
                            <td>{{ $article->price }}</td>
                            <td>{{ $article->ht_price }}</td>
                            <td>{{ $article->ttc_price }}</td>
                            <td>{{ $article->conditioning->name }}</td>
                            <td>{{ \Carbon\Carbon::parse($article->created_at)->format("d-m-Y H:i:s") }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Référence</th>
                        <th>Designation</th>
                        <th>Prix</th>
                        <th>Prix HT</th>
                        <th>Prix TTC</th>
                        <th>Conditionement</th>
                        <th>Ajoute le</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col">
        <!-- Modal -->
        <div class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ajouter un article</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <form action="{{ route('articles.store') }}" method="post">
                        <div class="modal-body">
                            @csrf
                            <div class="border p-4 rounded">
                                <div class="card-title d-flex align-items-center">
                                    <h5 class="mb-0 text-info">ARTICLES</h5>
                                </div>
                                <hr/>
                                <div class="row mb-3">
                                    <label for="reference" class="col-sm-3 col-form-label">Reference</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('reference') }}" min="0" name="reference"
                                               class="form-control" id="reference" placeholder="Reference" required>
                                        @error('reference')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="name" class="col-sm-3 col-form-label">Nom</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('name') }}" min="0" name="name"
                                               class="form-control" id="name" placeholder="Nom" required>
                                        @error('name')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="name" class="col-sm-3 col-form-label">Prix</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('price') }}" min="0" name="price"
                                               class="form-control" id="price" placeholder="Prix" required>
                                        @error('price')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="ht_price" class="col-sm-3 col-form-label">Prix HTC</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('ht_price') }}" min="0" name="ht_price"
                                               class="form-control" id="price" placeholder="Prix HTC" required>
                                        @error('ht_price')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="ttc_price" class="col-sm-3 col-form-label">Prix TTTC</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{ old('ttc_price') }}" min="0" name="ttc_price"
                                               class="form-control" id="price" placeholder="Prix TTC" required>
                                        @error('ttc_price')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="conditioning_id" class="col-sm-3 col-form-label">Conditionement</label>
                                    <div class="col-sm-9">
                                        <select name="conditioning_id" id="conditioning_id" class="form-control">
                                            <option value="">Choisir...</option>
                                            @foreach($conditionings as $conditioning)
                                                <option
                                                    value="{{ $conditioning->id }}">{{ $conditioning->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('$conditioning_id')
                                        <div class="text-danger small">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                            <button type="submit" class="btn btn-info px-5 text-white">Enregisstrer</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>


@endsection
