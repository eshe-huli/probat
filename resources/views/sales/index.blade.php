@extends('layouts.app')

@section('title')
    Users
@endSection

@section('breadcrumb')
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Gestion stock ventes</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Liste ventes</li>
                </ol>
            </nav>
        </div>
    </div>
    <!--end breadcrumb-->
@endsection


@section('content')
    <h6 class="mb-0 text-uppercase">Liste des clients</h6>
    <hr/>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="example2" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Articles</th>
                        <th>Regime</th>
                        <th>Clients</th>
                        <th>Prix</th>
                        <th>Prix HTC</th>
                        <th>Prix TTC</th>
                        <th>Payé</th>
                        <th>Mode paiement</th>
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sales as $sale)
                        <tr>
                            <td>{{$sale->designation}}</td>
                            <td>{{$sale->regime}}</td>
                            <td>{{$sale->customer->name}}</td>
                            <td>{{$sale->price}}</td>
                            <td>{{$sale->ht_price}}</td>
                            <td>{{$sale->ttc_price}}</td>
                            <td>{{$sale->paid}}</td>
                            <td>{{$sale->paymentMode->name}}</td> <td>
                                @if(($sale->ttc_price - $sale->paid) <=0)
                                    <span class="badge bg-gradient-quepal text-white shadow-sm w-100">Paid</span>
                                @else
                                    <span class="badge bg-gradient-bloody text-white shadow-sm w-100">Failed</span>
                                @endIf
                            </td>
                            <td>{{ \Carbon\Carbon::parse($sale->created_at)->format("d-m-Y H:i:s") }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Articles</th>
                        <th>Regime</th>
                        <th>Clients</th>
                        <th>Prix</th>
                        <th>Prix HTC</th>
                        <th>Prix TTC</th>
                        <th>Payé</th>
                        <th>Mode paiement</th>
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
