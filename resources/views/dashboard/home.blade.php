@extends('layouts.app')

@section('title')
    Home
@endsection


@section('content')
    <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
        <div class="col">
            <div class="card radius-10 border-start border-0 border-3 border-info">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div>
                            <p class="mb-0 text-secondary">Total Ventes</p>
                            <h4 class="my-1 text-info">{{number_format($sales->sum('ttc_price'),0,',', '.')}} CFA</h4>
                        </div>
                        <div class="widgets-icons-2 rounded-circle bg-gradient-scooter text-white ms-auto">
                            <i class='bx bxs-cart'></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card radius-10 border-start border-0 border-3 border-danger">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div>
                            <p class="mb-0 text-secondary">Total des Gains</p>
                            <h4 class="my-1 text-danger">{{number_format($sales->sum('paid'),0,',', '.')}} CFA</h4>
                        </div>
                        <div class="widgets-icons-2 rounded-circle bg-gradient-bloody text-white ms-auto"><i
                                class='bx bxs-wallet'></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card radius-10 border-start border-0 border-3 border-success">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div>
                            <p class="mb-0 text-secondary">Impeyer</p>
                            <h4 class="my-1 text-success">{{ number_format($sales->sum('ttc_price') - $sales->sum('paid'), 0, ',', '.') }} CFA</h4>
                        </div>
                        <div class="widgets-icons-2 rounded-circle bg-gradient-ohhappiness text-white ms-auto">
                            <i class='bx bxs-bar-chart-alt-2'></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card radius-10 border-start border-0 border-3 border-warning">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div>
                            <p class="mb-0 text-secondary">Total Clients</p>
                            <h4 class="my-1 text-warning">{{$customersCount}}</h4>
                        </div>
                        <div class="widgets-icons-2 rounded-circle bg-gradient-blooker text-white ms-auto">
                            <i class='bx bxs-group'></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card radius-10">
        <div class="card-body">
            <div class="d-flex align-items-center">
                <div>
                    <h6 class="mb-0">Commandes récentes</h6>
                </div>
                <div class="dropdown ms-auto">
                    <a class="dropdown-toggle dropdown-toggle-nocaret" href="#" data-bs-toggle="dropdown"><i
                            class='bx bx-dots-horizontal-rounded font-22 text-option'></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="javascript:;">Action</a>
                        </li>
                        <li><a class="dropdown-item" href="javascript:;">Another action</a>
                        </li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="javascript:;">Something else here</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-middle mb-0">
                    <thead class="table-light">
                    <tr>
                        <th>Articles</th>
                        <th>Regime</th>
                        <th>Clients</th>
                        <th>Prix</th>
                        <th>Prix HTC</th>
                        <th>Prix TTC</th>
                        <th>Payé</th>
                        <th>Mode paiement</th>
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sales as $sale)
                        <tr class="text-left">
                            <td>{{$sale->designation}}</td>
                            <td>{{$sale->regime}}</td>
                            <td>{{$sale->customer->name}}</td>
                            <td>{{$sale->price}}</td>
                            <td>{{$sale->ht_price}}</td>
                            <td>{{$sale->ttc_price}}</td>
                            <td>{{$sale->paid}}</td>
                            <td>{{$sale->paymentMode->name}}</td>
                            <td>
                                @if(($sale->ttc_price - $sale->paid) <=0)
                                    <span class="badge bg-gradient-quepal text-white shadow-sm w-100">Paid</span>
                                @else
                                    <span class="badge bg-gradient-bloody text-white shadow-sm w-100">Failed</span>
                                @endIf
                            </td>
                            <td>{{ \Carbon\Carbon::parse($sale->created_at)->format("d-m-Y H:i:s") }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
