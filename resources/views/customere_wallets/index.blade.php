@extends('layouts.app')

@section('title')
    Users
@endsection

@section('breadcrumb')
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Compte client</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Nouveaux clients</li>
                </ol>
            </nav>
        </div>
    </div>
    <!--end breadcrumb-->
@endsection


@section('content')
    <h6 class="mb-0 text-uppercase">Liste des portefeuilles</h6>
    <hr/>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="example2" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Balance</th>
                        <th>Caution</th>
                        <th>Plafond créance</th>
                        <th>Ajouté le</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($wallets as $wallet)
                        <tr>
                            <td>{{ $wallet->name }}</td>
                            <td>{{ $wallet->balance }}</td>
                            <td>{{ $wallet->caution }}</td>
                            <td>{{ $wallet->max_threshold }}</td>
                            <td>{{ \Carbon\Carbon::parse($wallet->created_at)->format("d-m-Y H:i:s") }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Nom</th>
                        <th>Balance</th>
                        <th>Caution</th>
                        <th>Plafond créance</th>
                        <th>Ajouté le</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
