@extends('layouts.app')

@section('title')
    Portefeuille client
@endsection

@section('breadcrumb')
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Compte client</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Ajouter portefeuille client</li>
                </ol>
            </nav>
        </div>
    </div>
    <!--end breadcrumb-->
@endsection


@section('content')
    <div class="row">
        <div class="col-xl-10 mx-auto">
            <hr/>
            <div class="card border-top border-0 border-4 border-info">
                <div class="card-body">
                    <form action="{{ route('customereWallets.store') }}" method="post">
                        @csrf
                        <div class="border p-4 rounded">
                            <div class="card-title d-flex align-items-center">
                                <div><i class="bx bxs-user me-1 font-22 text-info"></i>
                                </div>
                                <h5 class="mb-0 text-info">Ajout d'un nouveau portefeuille client</h5>
                            </div>
                            <hr/>
                            <div class="row mb-3">
                                <label for="name" class="col-sm-3 col-form-label">Nom</label>
                                <div class="col-sm-9">
                                    <input type="text" value="{{ old('name') }}" min="0" name="name" class="form-control" id="name" placeholder="Nom" required>
                                    @error('name')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="caution" class="col-sm-3 col-form-label">CAUTION</label>
                                <div class="col-sm-9">
                                    <input type="number" value="{{ old('caution') }}" min="0" name="caution" class="form-control" id="caution" placeholder="caution" required>
                                    @error('caution')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="threshold" class="col-sm-3 col-form-label">PLAFOND CREANCE</label>
                                <div class="col-sm-9">
                                    <input type="number" value="{{ old('threshold') }}" min="0" name="max_threshold" class="form-control" id="threshold" placeholder="plafond créance" required>
                                    @error('threshold')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="balance" class="col-sm-3 col-form-label">BALANCE</label>
                                <div class="col-sm-9">
                                    <input type="number" value="{{ old('balance') }}" min="0" name="balance" class="form-control" id="balance" placeholder="balance" required>
                                    @error('balance')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    <button type="submit" class="btn btn-info px-5 text-white">Ajouter</button>
                                    <button type="reset" class="btn btn-info px-5 text-white">Annuler</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
