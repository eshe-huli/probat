@extends('layouts.app')

@section('title')
    Articles
@endsection

@section('breadcrumb')
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Liste articles</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Liste articles</li>
                </ol>
            </nav>
        </div>
    </div>
    <!--end breadcrumb-->
@endsection


@section('content')
    <h6 class="mb-0 text-uppercase">Liste des articles</h6>
    <hr/>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="example2" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Référence</th>
                        <th>Designation</th>
                        <th>Prix</th>
                        <th>Prix HT</th>
                        <th>Prix TTC</th>
                        <th>Conditionement</th>
                        <th>Ajoute le</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articles as $article)
                        <tr>
                            <td>{{ $article->reference }}</td>
                            <td>{{ $article->designation }}</td>
                            <td>{{ $article->price }}</td>
                            <td>{{ $article->ht_price }}</td>
                            <td>{{ $article->ttc_price }}</td>
                            <td>{{ $article->conditioning->name }}</td>
                            <td>{{ \Carbon\Carbon::parse($article->created_at)->format("d-m-Y H:i:s") }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Référence</th>
                        <th>Designation</th>
                        <th>Prix</th>
                        <th>Prix HT</th>
                        <th>Prix TTC</th>
                        <th>Conditionement</th>
                        <th>Ajoute le</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
