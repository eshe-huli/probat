<?php

namespace App\Services;

use App\Models\Article;
use App\Models\Category;
use App\Models\Inventory;
use App\Models\Supplier;

class InventoryService
{

    public static function getItem(Inventory $inventory)
    {
        return Inventory::where([['id', $inventory->id]])->with(['article', 'supplier', 'category'])->first();
    }

    public static function setItem(Article $article, Supplier $supplier, Category $category)
    {
        return [];
    }
}
