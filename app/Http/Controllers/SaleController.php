<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Customer;
use App\Models\Sale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SaleController extends Controller
{

    public function index()
    {
        $sales = Sale::all();
        return view('sales.index', compact('sales'));
    }


    public function create()
    {
        $customers = Customer::all();
        $articles = Article::all();
        $wallets = Sale::all();

        return view('sales.create', compact(['wallets', 'customers', 'articles']));
    }


    public function store(StoreClientRequest $request)
    {
        try {
            $data = $request->validated();

            $data["reference"] = Str::upper(Str::random(8));

            Sale::create($data);

            toastSuccess("Vente effectué");

            return back();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            toastError("Une erreur est survenue");
            return back();
        }
    }

}
