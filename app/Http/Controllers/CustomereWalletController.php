<?php

namespace App\Http\Controllers;

use App\Models\CustomereWallet;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CustomereWalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $wallets = CustomereWallet::all();

        return view('customere_wallets.index', compact('wallets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customere_wallets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "balance" => "required",
            "caution" => "required",
            "max_threshold" => "required",
            "name" => "required",
        ], [
            "balance.required" => "La balance est requise",
            "caution.required" => "La caution est requise",
            "max_threshold.required" => "Le plafond créance est requis",
            "name.required" => "Le nom est requis",
        ]);

        if ($validator->fails()) {
            Log::error("Erreur lors de la création d'un nouveau portefeuille client : " . json_encode($request->all()));
            toastWarning("Veuillez remplir correctement les champs");
            return back()->withErrors($validator)->withInput();
        }

        try {
            $data = $request->except("_token", "_method");
            CustomereWallet::create($data);
            toastSuccess("Portefeuille créé avec succès");
            return redirect()->route("customereWallets.index");
        } catch (\Exception $e) {
            Log::error("Une erreur est surveune " . $e->getMessage());
            toastError("Une erreur est survenue, veuillez réessayer ou contacter l'administrateur.");
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $wallet = CustomereWallet::findOrFail($id);

        return view('customere_wallets.edit', compact("wallet"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $wallet = CustomereWallet::findOrFail($id);
            $wallet->delete();
            return back();
        } catch (\Exception $e) {
            toastError("Une erreur est survenue");
            Log::error($e->getMessage());
            return back();
        }
    }
}
