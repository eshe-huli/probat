<?php

namespace App\Http\Controllers;

use App\Models\Conditioning;
use Illuminate\Http\Request;

class ConditioningController extends Controller
{
    public function index()
    {
        $conditionings = Conditioning::all();
        return view('conditionings.index', compact('conditionings'));
    }


    public function store(Request $request)
    {
        $data = $request->get('name');
        Conditioning::create(['name' => $data]);
        return redirect()->route('conditionings.index');
    }
}
