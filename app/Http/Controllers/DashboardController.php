<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Sale;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $customersCount =  Customer::count();
        $sales= Sale::all();
        return view('dashboard.home', compact(['sales', 'customersCount']));
    }
}
