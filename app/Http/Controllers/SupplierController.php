<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
        $suppliers = Supplier::all();
        return view('suppliers.index', compact('suppliers'));
    }

    public function store(Request $request)
    {
        $data = $request->only(['reference', 'name', 'location','postal_code','phone', 'phone2', 'fax', 'email']);
        Supplier::create($data);
        return redirect()->route('suppliers.index');
    }
}
