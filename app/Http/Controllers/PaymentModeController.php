<?php

namespace App\Http\Controllers;

use App\Models\PaymentMode;
use Illuminate\Http\Request;

class PaymentModeController extends Controller
{
    public function index()
    {
        $paymentModes = PaymentMode::all();
        return view('paymentMethods.index', compact('paymentModes'));
    }

    public function store(Request $request)
    {
        $data = $request->get('name');
        PaymentMode::create(['name' => $data]);
        return redirect()->route('paymentModes.index');
    }
}
