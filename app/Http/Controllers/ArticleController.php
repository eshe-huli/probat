<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Conditioning;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(){
        $articles =  Article::all();
        $conditionings =  Conditioning::all();
        return view('articles.index',  compact(['articles', 'conditionings']));
    }

    public function store(Request $request)
    {
        $data = $request->only(['reference', 'name', 'price','ht_price', 'ttc_price', 'conditioning_id']);
        Article::create($data);
        return redirect()->route('articles.index');
    }
}
