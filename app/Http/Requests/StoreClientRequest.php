<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //"customere_wallet_id" => "required",
            "community_number" => "required",
            "email" => "required|email|unique:customers",
            "phone" => "required|unique:customers",
            "phone2" => "required|unique:customers",
            "caution" => "required",
            "threshold" => "required",
            "postal_code" => "required",
            "name" => "required",
            "location" => "required",
        ];
    }

    public function messages()
    {
        return [
            "reference.required" => "Ce champ est requis.",
            "customere_wallet_id.required" => "Ce champ est requis.",
            "community_number.required" => "Ce champ est requis.",
            "email.required" => "Ce champ est requis.",
            "email.email" => "Email au mauvais format.",
            "phone.required" => "Ce champ est requis.",
            "phone2.required" => "Ce champ est requis.",
            "caution.required" => "Ce champ est requis.",
            "threshold.required" => "Ce champ est requis.",
            "postal_code.required" => "Ce champ est requis.",
            "name.required" => "Ce champ est requis.",
            "location.required" => "Ce champ est requis.",
        ];
    }
}
