<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    public function paymentMode(){
        return $this->belongsTo(PaymentMode::class);
    }
    public function customer(){
        return $this->belongsTo(Customer::class);
    }
}
