<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['reference', 'name', 'price', 'ht_price', 'ttc_price', 'conditioning_id'];

    public function inventories()
    {
        return $this->hasMany(Inventory::class);
    }

    public function conditioning()
    {
        return $this->belongsTo(Conditioning::class);
    }
}
