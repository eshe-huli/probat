<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;

    protected $fillable = ['reference', 'name', 'location', 'postal_code', 'phone', 'phone2', 'fax', 'email'];

    public function inventories()
    {
        return $this->hasMany(Inventory::class);
    }
}
